package com.nagz.sm.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Not a valid session.") 
public class SessionNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 138858958356359971L;

}
