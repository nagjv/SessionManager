## DynamoDB & Spring Boot example


_Disclaimer: This is not a tutorial for Spring Boot or RESTful web services or DynamoDB. This is just an example for anyone looking for simple working sample of DynamoDB with Spring Boot._

##### Overview
I was in need of DynamoDB/Spring Boot example for an ongoing personal project. Initially, I spent time online researching for a working example but they were either outdated or confusing with unnecessary code. So, I compiled my learnings/observations into this example. Hopefully you will find it useful.

##### Prerequisites
Before you clone & run this example, make sure you have below prerequisites.
```
Java 8
Maven
AWS Account
DynamoDB table user_sessions
    3 string columns (sid, username, timestamp)
    sid is your Partition key
    sid is auto generated
Configure AWS locally
```

##### Clone & run
```sh
$ git clone git@gitlab.com:nagjv/SessionManager.git
$ mvn spring-boot:run
```

##### Sample request & response
```javascript
POST: localhost:7070/sessions/
    Request
    {"username": "helloworld", "timestamp": "000000000"}
    Response
    {"sid":"3c51-47f6-843d-c3e57","username":"helloworld","timestamp":"000000000"}
    
GET: localhost:7070/sessions/3c51-47f6-843d-c3e57
    Response
    {"sid":"3c51-47f6-843d-c3e57","username":"helloworld","timestamp":"000000000"}
```
##### References
* [AWS Docs Link 1](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBMapper.html)
* [AWS Docs Link 2](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/CodeSamples.Java.html)
* [Git sample](https://github.com/michaellavelle/spring-data-dynamodb)